Role Name
=========

A role that installs the Open ASFA docker stack, test environment

Role Variables
--------------

The most important variables are listed below:

``` yaml
open_asfa_compose_dir: '/srv/open_asfa_stack_test'
open_asfa_docker_stack_name: 'open-asfa-test'
open_asfa_docker_service_server_name: 'asfa-server-test'
open_asfa_docker_service_client_name: 'asfa-client-test'
open_asfa_docker_server_image: 'lucabrl01/asfa-server'
open_asfa_docker_client_image: 'lucabrl01/asfa-client'
open_asfa_docker_network: 'open_asfa_test_net'
open_asfa_behind_haproxy: True
open_asfa_haproxy_public_net: 'haproxy-public'
# DB
open_asfa_db_as_container: True
open_asfa_db_pg_version: 12
open_asfa_db_image: 'postgres:{{ open_asfa_db_pg_version }}-alpine'
open_asfa_db_host: 'pg'
open_asfa_db_port: 5432
#open_asfa_db_pwd: 'set it in a vault file'
open_asfa_db_name: 'asfadb'
open_asfa_db_user: 'asfadb_user'
open_asfa_db_volume: 'asfa_pg_data'
open_asfa_db_allowed_hosts:
  - '127.0.0.1/8'
open_asfa_db_constraints: '[node.labels.pg_data==asfa_server]'

open_asfa_psql_db_data: 
  - { db_host: '{{ open_asfa_db_host }}', pgsql_version: '{{ open_asfa_db_pg_version }}', name: '{{ open_asfa_db_name }}', encoding: 'UTF8', user: '{{ open_asfa_db_user }}', roles: 'CREATEDB,NOSUPERUSER', pwd: '{{ open_asfa_db_pwd }}', allowed_hosts: '{{ open_asfa_db_allowed_hosts }}' }
```

Dependencies
------------

A docker swarm cluster is required

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
